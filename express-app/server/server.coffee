require 'coffee-script/register'
express = require 'express'
routes = require '../routes/routes'
config = require('../../brunch-config').config
path = require 'path'
app = express()

# all env
app.set "port", config.server.port
app.set "views", __dirname + "/../views"
app.set "view engine", "jade"
app.use express.logger("dev")
app.use express.cookieParser()
app.use express.bodyParser()
app.use express.methodOverride()
app.use express.static(path.join(__dirname, "../../www"))
app.use app.router

# dev
if app.get("env") is "development"
  console.log "development"
  app.use express.errorHandler()

# prod
if app.get("env") is "production"
  console.log "production"

# routes
routes app

# start server
app.listen app.get("port")

module.exports = app